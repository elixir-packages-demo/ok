# ok

![Hex.pm](https://img.shields.io/hexpm/dw/ok)
Elegant error/exception handling in Elixir, with result monads. [ok](https://hex.pm/packages/ok)

# Similar packages
* ![exceptional](https://img.shields.io/hexpm/dw/exceptional) exceptional
* ![ok](https://img.shields.io/hexpm/dw/ok) ok
* ![towel](https://img.shields.io/hexpm/dw/towel) towel
* ![witchcraft](https://img.shields.io/hexpm/dw/witchcraft) witchcraft
* ![monadex](https://img.shields.io/hexpm/dw/monadex) monadex
